Instance: ch-ehealth-codesystem-role 
InstanceOf: CodeSystem 
Usage: #definition 
* url = "https://isels1.gitlab.io/terminolo-git-test/CodeSystem-ch-ehealth-codesystem-role" 
* name = "ch-ehealth-codesystem-role" 
* title = "EPR Role" 
* status = #active 
* content = #complete 
* version = "0.1.1" 
* description = "EPR Document.author.authorRole" 
* identifier[0].use = #official 
* identifier[0].system = "urn:ietf:rfc:3986" 
* identifier[0].value = "urn:oid:2.16.756.5.30.1.127.3.10.1.1.3" 
* date = "2021-04-01" 
* count = 5 
* #ASS "Assistant"
* #ASS ^definition = None
* #ASS ^designation[0].language = #de-CH 
* #ASS ^designation[0].value = "Hilfsperson" 
* #ASS ^designation[1].language = #rm-CH 
* #ASS ^designation[1].value = "Persuna d'agid" 
* #ASS ^designation[2].language = #fr-CH 
* #ASS ^designation[2].value = "Auxiliaire" 
* #ASS ^designation[3].language = #it-CH 
* #ASS ^designation[3].value = "Persona ausiliara" 
* #HCP "Healthcare professional"
* #HCP ^definition = None
* #HCP ^designation[0].language = #de-CH 
* #HCP ^designation[0].value = "Gesundheitsfachperson" 
* #HCP ^designation[1].language = #rm-CH 
* #HCP ^designation[1].value = "Persuna spezialisada dal sectur da sanadad" 
* #HCP ^designation[2].language = #fr-CH 
* #HCP ^designation[2].value = "Professionnel de la santï¿½" 
* #HCP ^designation[3].language = #it-CH 
* #HCP ^designation[3].value = "Professionista della salute" 
* #PAT "Patient"
* #PAT ^definition = None
* #PAT ^designation[0].language = #de-CH 
* #PAT ^designation[0].value = "Patient" 
* #PAT ^designation[1].language = #rm-CH 
* #PAT ^designation[1].value = "Pazient" 
* #PAT ^designation[2].language = #fr-CH 
* #PAT ^designation[2].value = "Patient" 
* #PAT ^designation[3].language = #it-CH 
* #PAT ^designation[3].value = "Paziente" 
* #REP "Representative"
* #REP ^definition = None
* #REP ^designation[0].language = #de-CH 
* #REP ^designation[0].value = "Stellvertretung" 
* #REP ^designation[1].language = #rm-CH 
* #REP ^designation[1].value = "Substituziun" 
* #REP ^designation[2].language = #fr-CH 
* #REP ^designation[2].value = "Reprï¿½sentant" 
* #REP ^designation[3].language = #it-CH 
* #REP ^designation[3].value = "Rappresentante" 
* #TCU "Technical user"
* #TCU ^definition = None
* #TCU ^designation[0].language = #de-CH 
* #TCU ^designation[0].value = "Technischer Benutzer" 
* #TCU ^designation[1].language = #rm-CH 
* #TCU ^designation[1].value = "Utilisader tecnic" 
* #TCU ^designation[2].language = #fr-CH 
* #TCU ^designation[2].value = "Utilisateur technique" 
* #TCU ^designation[3].language = #it-CH 
* #TCU ^designation[3].value = "Utente tecnico" 
